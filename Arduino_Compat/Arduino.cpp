#include "Arduino.h"

auto t0 = Time::now();

unsigned long millis() {
  auto t1 = Time::now();
  fsec fs = t1 - t0;
  ms d = std::chrono::duration_cast<ms>(fs);
  return (unsigned long) d.count(); 
}

//Compat arduino
int main() {
  setup();
  for(;;) {
    loop();
  }
}
