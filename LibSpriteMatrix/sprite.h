#ifndef Sprite_h
#define Sprite_h

//For pc
#ifndef ARDUINO 
    #include <Arduino.h>
#endif

#include <Adafruit_GFX.h>

class Sprite : public Adafruit_GFX
{
  public:
    Sprite(uint8_t width, uint8_t height, int16_t x, int16_t y, uint8_t fps);
    ~Sprite(void);

    uint16_t Color(uint8_t r, uint8_t g, uint8_t b);
    
    void setState(uint8_t state);
    
    uint16_t *getBuffer(void);
    uint8_t getState();
    uint8_t getHeight();
    uint8_t getWidth();
    int16_t getLeft();
    int16_t getTop();

    void drawPixel(int16_t x, int16_t y, uint16_t color);
    //void fillScreen(uint16_t color);
    bool update(unsigned long timer);
    virtual bool draw() = 0;
  
  protected:
    uint16_t *_buffer;
    uint8_t _fps=0;
    int16_t _x;
    int16_t _y;
    uint8_t _state=0;
    unsigned long _lastUpdate=-1000;
};

#endif
